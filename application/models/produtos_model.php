<?php 
/**
* model de produtos
*/
class Produtos_model extends CI_Model{
	
	public function busca($id){
		return $this->db->get_where("produtos", array(
			"id_produtos" => $id
		))->row_array();
	}

	public function buscaTodos(){
		return $this->db->get("produtos")->result_array();
	}

	public function salva($produto){
		$this->db->insert("produtos", $produto);
	}
}







?>