<?php

class Usuarios_model extends CI_Model {

	public function salva($usuarios){
		$this->db->insert("usuarios", $usuarios);
	}	

	public function buscaPorEmailESenha($email, $senha){
		$this->db->where("user_email", $email);
		$this->db->where("user_senha", $senha);
		$usuarios = $this->db->get("usuarios")->row_array();
		return $usuarios;
	}

}

/* End of file usuarios_model.php */
/* Location: ./application/models/usuarios_model.php */