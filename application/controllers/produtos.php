<?php if ( ! defined('BASEPATH')) exit( 'No direct script access allowed');

class Produtos extends CI_Controller{

	public function mostra($id){
		//$id = $this->input->get("id_produtos");
		$this->load->model("produtos_model");
		$produto = $this->produtos_model->busca($id);
		$dados = array("produto" => $produto);
		$this->load->helper("typography");
		$this->load->view("produtos/mostra", $dados);
	}	
	
	public function index(){
		// $this->output->enable_profiler(true);
		$this->load->model("produtos_model");
		$produtos= $this->produtos_model->buscaTodos();

		$dados = array("produtos" => $produtos );
		$this->load->helper("currency");
		$this->load->view("produtos/index.php", $dados);
	}
	public function formulario(){
		$this->load->view("produtos/formulario");

	}
	public function novo(){
		$this->load->library("form_validation");
		$this->form_validation->set_rules("produtos_name", "Nome do Produto", "required|min_length[5]|callback_nao_tenha_a_palavra_melhor");
		$this->form_validation->set_rules("produtos_preco", "Preço do produto", "required|min_length[5]");
		$this->form_validation->set_rules("produtos_descricao", "Descrição do Produto", "trim|required|min_length[10]");
		$this->form_validation->set_error_delimiters("<p class='alert alert-danger'>", "</p>");

		$success = $this->form_validation->run();
		if ($success) {
			$usuario_logado = $this->session->userdata("usuario_logado");
			$produto = array(
				"produtos_name" => $this->input->post("produtos_name"),
				"produtos_descricao" => $this->input->post("produtos_descricao"),
				"produtos_preco" => $this->input->post("produtos_preco"),
				//"id_user" => $usuario_logado["id"]
			);
			$this->load->model("produtos_model");
			$this->produtos_model->salva($produto);
			$this->session->set_flashdata("success", "Novo produto adicionado");
			redirect('/');
		}else{
			$this->load->view("produtos/formulario");
		}
	}

	public function nao_tenha_a_palavra_melhor($nome){
		$posicao = strpos($nome, "melhor");
		if ($posicao != FALSE) {
			return TRUE;
		}else{
			$this->form_validation->set_message("nao_tenha_a_palavra_melhor", "O campo '%s' não pode conter a palavra 'melhor'. ");
			return FALSE;
		}
	}

}
?>