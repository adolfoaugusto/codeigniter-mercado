<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function autenticar(){
		$this->load->model("usuarios_model");

		$email = $this->input->post("user_email");
		$senha = md5($this->input->post("user_senha"));
		$usuarios = $this->usuarios_model->buscaPorEmailESenha($email, $senha);
		if ($usuarios) {
			$this->session->set_userdata( "usuario_logado", $usuarios);
			$this->session->set_flashdata("success", "Logado com sucesso");
			//$dados = array("mensagem" =>"Logado com sucesso");
		} else {
			$this->session->set_flashdata("danger", "Usuário ou senha inválido.");
			//$dados = array("mensagem" =>"Usuário ou senha inválido.");
		}
		//$this->load->view("login/autenticar", $dados);
		redirect('/');
	}

	public function logout(){
		$this->session->unset_userdata("usuario_logado");
		$this->session->set_flashdata("success", "Usuário deslogado.");
		redirect('/');
		//$this->load->view("login/logout");
	}
	public function logout() {
		$this->session->unset_userdata("usuario_logado");
		$this->session->set_flashdata("success", "Deslogado com sucesso");
		redirect('/');
	}
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */