<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	public function novo(){
		// $this->output->enable_profiler(true);
		
		$usuarios = array(
			"user_name" => $this->input->post("user_name"),
			"user_email" => $this->input->post("user_email"),
			"user_senha" => md5($this->input->post("user_senha"))
		);

		$this->load->model("usuarios_model");
		$this->usuarios_model->salva($usuarios);
		$this->load->view("usuarios/novo");
		
	}

}
/* End of file usuarios.php */
/* Location: ./application/controllers/usuarios.php */