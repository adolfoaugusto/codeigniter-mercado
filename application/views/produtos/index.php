<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Produtos</title>

	<link rel="stylesheet" href="<?= base_url("css/bootstrap.css"); ?>">
</head>
<body>
	<div class="container">
		<!-- mensagem de alerta de session -->
		<?php if($this->session->flashdata("success")): ?>
			<p class="alert alert-success"><?= $this->session->flashdata("success") ?></p>
		<?php endif ?>
		<!-- mensagem de alerta de session -->
		<?php if($this->session->flashdata("danger")): ?>
			<p class="alert alert-danger"><?= $this->session->flashdata("danger") ?></p>
		<?php endif ?>

		<!-- Listagem de Produtos -->
		<h1>Produtos</h1>
		<table class="table">
		<?php foreach ($produtos as $produto): ?>
			<tr>
				<td><?= anchor("produtos/{$produto['id_produtos']}", $produto["produtos_name"]); ?></td>
				<td><?= character_limiter($produto['produtos_descricao'], 15)?></td>
				<td><?= numeroEmReais($produto["produtos_preco"])?></td>
			</tr>
		<?php endforeach; ?>
		</table>
	
	<!-- Cadastro de Produto -->
	<?php if( $this->session->userdata("usuario_logado") ) : ?>
		<?= anchor('produtos/formulario', 'Novo Produto', array( "class"=>"btn btn-primary")); ?>
		<?= anchor('login/logout', 'Logout', array( "class"=>"btn btn-primary")); ?>
	<?php else : ?>
	
	<!-- Login de Usuário -->
	<h1>Login</h1>
	<?php 
		echo form_open("login/autenticar");
		echo form_label("Email", "user_email");
		echo form_input(array(
			"name" => "user_email", 
			"id" => "email",
			"class" => "form-control",
			"maxlength" => "255"));

		echo form_label("Senha", "user_senha");
		echo form_password(array(
			"name" => "user_senha", 
			"id" => "senha",
			"class" => "form-control",
			"maxlength" => "255"));
		echo form_button(array(
			"class" => "btn btn-primary",
			"content" => "Logar",
			"type" => "submit"));
		echo form_close();
	?>
	
	<h1>Cadastro</h1>
	<?php 
		echo form_open("usuarios/novo"); 
		echo form_label("Nome", "user_name");
		echo form_input(array(
			"name" => "user_name", 
			"class" => "form-control",
			"maxlength" => "255"));

		echo form_label("Email", "user_email");
		echo form_input(array(
			"name" => "user_email", 
			"id" => "email",
			"class" => "form-control",
			"maxlength" => "255"));

		echo form_label("Senha", "user_senha");
		echo form_password(array(
			"name" => "user_senha", 
			"id" => "senha",
			"class" => "form-control",
			"maxlength" => "255"));
		echo form_button(array(
			"class" => "btn btn-primary",
			"content" => "Cadastrar",
			"type" => "submit"));
	
	?>
	</div>
	<?php endif ?>

	<!-- <hr>
	 <pre>
		<?php // var_dump($produtos); ?>
	</pre> -->

</body>
</html>