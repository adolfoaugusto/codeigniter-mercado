<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Cadastro de produto</title>
	<link rel="stylesheet" href="<?= base_url("css/bootstrap.css") ?>">
</head>
<body>
<div class="container">
	<h1>Cadastrar novo produto</h1>
<?php 
	echo form_open("produtos/novo");
	echo form_label("Nome do Produto", "Nome");
	echo form_input(array(
		"name" => "produtos_name", 
		"class" => "form-control",
		"id" => "nome",
		"maxlength" => "255",
		"value" =>set_value("produtos_name", "")
	));
	echo form_error("produtos_name");
	echo form_label("Preço", "Preço");
	echo form_input(array(
		"name" => "produtos_preco", 
		"class" => "form-control",
		"id" => "preco",
		"maxlength" => "255",
		"type" =>"number",
		"value" =>set_value("produtos_preco", "")
	));
	echo form_error("produtos_preco");
	echo form_textarea(array(
		"name" => "produtos_descricao", 
		"class" => "form-control",
		"id" => "descricao",
		"value" =>set_value("produtos_descricao", "")
	));
	echo form_error("produtos_descricao");
	echo form_button(array(
		"class" => "btn btn-primary",
		"content" => "Cadastrar",
		"type" => "submit"));
echo form_close();
?>
	
</div>
</body>
</html>