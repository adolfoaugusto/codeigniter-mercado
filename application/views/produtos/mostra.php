<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Produtos detalhados</title>

	<link rel="stylesheet" href="<?= base_url("css/bootstrap.css"); ?>">
</head>
<body>
	<div class="container">
	<h1><?= $produto["produtos_name"]?></h1><br>	
	Preço: <?= $produto["produtos_preco"]?><br>	
	<?= auto_typography(html_escape($produto["produtos_descricao"]))?><br>	
	<h2>Compre Agora mesmo</h2>
	<?php 
		echo form_open("vendas/nova");

		echo form_label('Data de entrega', 'data_de_entrega'); 
		echo form_input(array(
			"name" => "data_de_entrega",
			"class" => "form-control",
			"id" => "data_de_entrega",
			"maxlength" => "255",
			"value" =>""
			));
		echo form_button(array(
			"class" => "btn btn-primary",
			"content" =>"Comprar", 
			"type" => "submit"
			));
		echo form_close();
	?>
	</div>
</body>
</html>	